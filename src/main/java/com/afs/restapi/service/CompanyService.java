package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    @Autowired
    private  CompanyRepository companyRepository;


    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public CompanyRepository getCompanyRepository() {
        return companyRepository;
    }

    public List<Company> getCompanyRepositoryCompanies() {
        return getCompanyRepository().getCompanies();
    }

    public Company getCompanyById(Integer companyId) {
        return companyRepository.getCompanyById(companyId);
    }

    public List<Employee> getEmployeesByCompanyId(Integer companyId) {
        return companyRepository.getEmployeesByCompanyId(companyId);
    }

    public List<Company> getCompaniesByPagination(Integer pageIndex, Integer pageSize) {
        return companyRepository.getCompaniesByPagination(pageIndex,pageSize);
    }

    public void addCompany(Company company) {
        companyRepository.addCompany(company);
    }

    public Company updateCompany(Integer companyId, Company company) {
        Company companyToUpdate = companyRepository.getCompanyById(companyId);
        companyRepository.updateCompanyAttributes(companyToUpdate,company);
        return companyToUpdate;
    }


    public boolean remove(Integer companyId) {
        return companyRepository.delete(companyId);
    }
}
package com.afs.restapi.service;

import com.afs.restapi.exception.AgeException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insertEmployee(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new AgeException();
        }
        if (employee.getAge() >= 30 && employee.getSalary() < 20000) {
            throw new AgeNotMatchSalaryException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);

    }

    public Employee getUpdate(int id, Employee employee) {
        Employee employeeToUpdate = employeeRepository.findById(id);
        return employeeRepository.update(employeeToUpdate, employee);
    }

    public void getDelete(int id) {
        Employee employeeDelete = employeeRepository.findById(id);
        employeeDelete.setStatus(false);
        employeeRepository.updateStatus(employeeDelete);
    }
}

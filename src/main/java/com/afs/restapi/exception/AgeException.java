package com.afs.restapi.exception;

public class AgeException extends RuntimeException{
    public AgeException() {
        super("age invalid");
    }
}

package com.afs.restapi.exception;

public class AgeNotMatchSalaryException extends RuntimeException{
    public AgeNotMatchSalaryException() {
        super("AgeNotMatchSalary");
    }
}

package com.afs.restapi;

import com.afs.restapi.exception.AgeException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class EmployeeServiceTest {

    private EmployeeRepository employeeRepository;
    private EmployeeService employeeService;

    @BeforeEach
    void setUp() {
        employeeRepository = Mockito.mock(EmployeeRepository.class);
        employeeService = new EmployeeService();
        employeeService.setEmployeeRepository(employeeRepository);
    }

    @Test
    public void should_throw_age_invalid_when_insert_given_age15_or_age70() {
        Employee childEmployee = new Employee("John", 15, "male", 1000);
        Employee oldEmployee = new Employee("A", 70, "male", 100000);

        assertThrows(AgeException.class, () -> employeeService.insertEmployee(childEmployee));
        assertThrows(AgeException.class, () -> employeeService.insertEmployee(oldEmployee));

        Mockito.verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    public void should_throw_age_not_match_salary_when_insert_given_age31_and_salary18000() {
        Employee AgeNotMatchSalaryEmployee = new Employee("A", 50, "male", 18000);

        assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.insertEmployee(AgeNotMatchSalaryEmployee));

        Mockito.verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule() {
        //given
        Employee employee = new Employee(1, "abc", 38, "male", 35000);
        Employee employeeShouldReturn = new Employee(1, "abc", 38, "male", 35000);
        employeeShouldReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeShouldReturn);

        //when
        Employee employeeSaved = employeeService.insertEmployee(employee);

        //then
        assertTrue(employeeSaved.isStatus());

        verify(employeeRepository).insert(argThat(employeeSave -> {
            assertTrue(employeeSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_return_employee_with_status_false_when_delete_employee_given_a_employee_match_rule() {
        //given
        Employee employeeShouldReturn = new Employee(1, "abc", 38, "male", 35000);
        employeeShouldReturn.setStatus(true);
        when(employeeRepository.findById(anyInt())).thenReturn(employeeShouldReturn);

        employeeService.getDelete(1);

        //when
        verify(employeeRepository).updateStatus(argThat(employeeDelete -> {
            assertFalse(employeeDelete.isStatus());
            return true;
        }));

    }

    @Test
    void should_throw_exception_when_update_given_inactive_employee() {
        //given
        Employee employee = new Employee(1, "abc", 38, "male", 35000);
        employee.setStatus(false);
        when(employeeRepository.findById(1)).thenThrow(new EmployeeNotFoundException());

        //when
        Employee newEmployee = new Employee(0, "", 0, "0", 36000);

        //then
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.getUpdate(1, newEmployee));
        Mockito.verify(employeeRepository, Mockito.times(0)).update(any(),any());

    }


}
